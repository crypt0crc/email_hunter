import requests
import argparse

def emails_validate(headers,API_KEY,file_emails):
	try:
		file = open(file_emails,"r")
		for email in file.readlines():
			url = "https://api.hunter.io:443/v2/email-verifier?email="+str(email).strip()+"&api_key="+str(API_KEY)
			data = requests.get(url, headers=headers).json()
			valid_emails=[]
			valid_email = data['data']['status']
			if str(valid_email) == "valid":
				print("[+]Valid: "+str(data['data']['email']))
				valid_emails.append(str(data['data']['email']))
	except:
		print("Failed!")

def emails_finder(headers,API_KEY,domain):
	try:
		url = "https://api.hunter.io/v2/domain-search?domain="+str(domain)+"&api_key="+str(API_KEY)
		data = requests.get(url, headers=headers).json()	
		emails_find = []
		for email in data['data']['emails']:
			print("[+] Email: "+str(email['value']))
			emails_find.append(str(email['value']))
	except:
		print("Failed!")

def main():

	#parse arguments
	parser = argparse.ArgumentParser()
	parser.add_argument("-d", "--domain", help="Domain to search emails", action="store")
	parser.add_argument("-f", "--file_emails", help="File with emails to check", action="store")
	args = parser.parse_args()

	API_KEY="API_KEY"

	headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0", "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8", "Accept-Language": "es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3", "Accept-Encoding": "gzip, deflate", "Referer": "https://hunter.io/", "Upgrade-Insecure-Requests": "1", "Sec-Fetch-Dest": "document", "Sec-Fetch-Mode": "navigate", "Sec-Fetch-Site": "same-site", "Sec-Fetch-User": "?1", "Te": "trailers", "Connection": "close"}
	

	file_emails = str(args.file_emails)
	domain = str(args.domain)
	if args.file_emails is not None:
		emails_validate(headers,API_KEY,args.file_emails)
	if args.domain is not None:
		emails_finder(headers,API_KEY,args.domain)
main()
# Email_Hunter

Simple script to find and validate emails from any company via hunter.io API

![Help](/images/emailhunter_help.png)

## Getting started

Please make an account at *hunter.io* , and grab your api key and insert inside "***API_KEY***" variable inside the python code.

## Installation
```
pip install requests
```
```
pip install argparse
```

## Usage

***Search for emails:***
```
python3 email_hunter.py -d test.com
```
***Validate Emails:***

```
python email_hunter.py -f mails_file.txt
```


